module a();
    wire x = 1'b1;
    wire y;
    buf mybuf (y, x);
endmodule
